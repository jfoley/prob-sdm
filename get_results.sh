for method in gsdm msdm nsdm nbsdm; do
  for split in $(seq 0 4); do
    FILE_NAME=$(grep "Mean_" ${method}.*_split${split}.*train.trecrun.out | sort -nrk2 | head -n 1 | awk '{print $1}' | awk -F':' '{print $1}' | sed 's/train/test/g');
    MAP=$(grep 'Mean_' ${FILE_NAME} | awk '{print $2}')
    echo ${FILE_NAME} ${MAP}
  done
done

