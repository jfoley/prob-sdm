#!/bin/bash

set -eu
GALAGO="sh ${HOME}/code/stable/galago-3-8/core/target/appassembler/bin/galago"

for x in gsdm msdm nsdm ql nbsdm; do
  ${GALAGO} eval metrics.json --judgments=data/robust04.qrels --baseline=final-trecruns/${x}.trecrun --details=true > final-galagoeval/${x}.galagoeval
done

mkdir -p ranklib-galagoeval
for x in gen.msdm gen.nsdm log.msdm log.nsdm; do
  ${GALAGO} eval metrics.json --judgments=data/robust04.qrels --baseline=ranklib-trecruns/${x}.trecrun --details=true > ranklib-galagoeval/${x}.galagoeval
done

