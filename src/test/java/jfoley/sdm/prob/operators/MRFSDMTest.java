package jfoley.sdm.prob.operators;

import ciir.jfoley.chai.lang.LazyPtr;
import jfoley.sdm.prob.IterUtils;
import org.junit.Test;
import org.lemurproject.galago.core.index.mem.MemoryIndex;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;

import static org.junit.Assert.assertEquals;

/**
 * @author jfoley.
 */
public class MRFSDMTest {
  public static LazyPtr<TagTokenizer> tok = new LazyPtr<>(TagTokenizer::new);

  public static Document makeDoc(String text) {
    return tok.get().tokenize(text);
  }

  @Test
  public void testMRFSDMShouldScoreTheSame() throws Exception {
    MemoryIndex memIndex = new MemoryIndex();
    memIndex.process(makeDoc("the quick brown fox"));
    memIndex.process(makeDoc("the quick brown dog"));
    memIndex.process(makeDoc("the quick brown dog eats some steak"));

    Parameters argp = Parameters.create();
    IterUtils.addToParameters(argp, "msdm", MRFSDM.class);
    LocalRetrieval ret = new LocalRetrieval(memIndex, argp);

    double mu = 1000;
    argp.put("mu", mu);
    argp.put("unimu", mu);
    argp.put("odmu", mu);
    argp.put("uwmu", mu);
    argp.put("uniw", 0.8);
    argp.put("odw", 0.15);
    argp.put("uww", 0.05);
    argp.put("processingModel", "rankeddocument");

    Results mResults = ret.transformAndExecuteQuery(StructuredQuery.parse("#msdm(quick brown)"), argp.clone());
    Results sResults = ret.transformAndExecuteQuery(StructuredQuery.parse("#sdm(quick brown)"), argp.clone());

    assertEquals(sResults.scoredDocuments.size(), mResults.scoredDocuments.size());
    for (int i = 0; i < sResults.scoredDocuments.size(); i++) {
      assertEquals(sResults.scoredDocuments.get(i).score, mResults.scoredDocuments.get(i).score, 0.000001);
    }
  }

  @Test
  public void testMRFSDMShouldScoreTheSame2() throws Exception {
    MemoryIndex memIndex = new MemoryIndex();
    memIndex.process(makeDoc("the quick brown fox"));
    memIndex.process(makeDoc("the quick brown dog"));
    memIndex.process(makeDoc("the quick brown dog eats some steak"));

    Parameters argp = Parameters.create();
    IterUtils.addToParameters(argp, "msdm", MRFSDM.class);
    LocalRetrieval ret = new LocalRetrieval(memIndex, argp);

    double mu = 1000;
    argp.put("mu", mu);
    argp.put("unimu", mu);
    argp.put("odmu", mu);
    argp.put("uwmu", mu);
    argp.put("uniw", 0.8);
    argp.put("odw", 0.15);
    argp.put("uww", 0.05);
    argp.put("processingModel", "rankeddocument");

    Results mResults = ret.transformAndExecuteQuery(StructuredQuery.parse("#msdm(quick brown dog)"), argp.clone());
    Results sResults = ret.transformAndExecuteQuery(StructuredQuery.parse("#sdm(quick brown dog)"), argp.clone());

    assertEquals(sResults.scoredDocuments.size(), mResults.scoredDocuments.size());
    for (int i = 0; i < sResults.scoredDocuments.size(); i++) {
      assertEquals(sResults.scoredDocuments.get(i).score, mResults.scoredDocuments.get(i).score, 0.000001);
    }
  }

}