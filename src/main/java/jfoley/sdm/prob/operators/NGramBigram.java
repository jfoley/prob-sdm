package jfoley.sdm.prob.operators;

import org.lemurproject.galago.core.retrieval.RequiredParameters;
import org.lemurproject.galago.core.retrieval.RequiredStatistics;
import org.lemurproject.galago.core.retrieval.iterator.*;
import org.lemurproject.galago.core.retrieval.processing.ScoringContext;
import org.lemurproject.galago.core.retrieval.query.AnnotatedNode;
import org.lemurproject.galago.core.retrieval.query.NodeParameters;
import org.lemurproject.galago.core.util.ExtentArray;

import java.io.IOException;

import static jfoley.sdm.prob.operators.FeatureUtil.logDirichlet;
import static jfoley.sdm.prob.operators.FeatureUtil.orderedWindow;

/**
 * @author jfoley.
 */
@RequiredStatistics(statistics = {"collectionLength"})
@RequiredParameters(parameters = {"unimu", "odmu"})
public class NGramBigram extends DisjunctionIterator implements ScoreIterator {
  private final LengthsIterator lengthsIter;
  private final ExtentIterator[] unigramIters;

  double term_bg[], od_bg[];
  private ExtentArray[] pos;

  private final double unigramMu;
  private final double bigramMu;

  public NGramBigram(NodeParameters parameters, LengthsIterator lengthsIter, ExtentIterator[] unigramIters) throws IOException {
    super(unigramIters);
    this.lengthsIter = lengthsIter;
    this.unigramIters = unigramIters;
    assert(unigramIters.length > 1);

    double collectionLength = parameters.getLong("collectionLength");
    this.unigramMu = parameters.getDouble("unimu");
    this.bigramMu = parameters.getDouble("odmu");

    calculateStats(collectionLength);
    this.pos = new ExtentArray[unigramIters.length];
  }

  private void calculateStats(double collectionLength) throws IOException {
    ScoringContext ctxt = new ScoringContext();

    long term_cf[], od_cf[];

    term_cf = new long[unigramIters.length];
    od_cf = new long[unigramIters.length-1];

    term_bg = new double[unigramIters.length];
    od_bg = new double[unigramIters.length-1];

    // note that 'this' is a disjunction iterator
    while(!this.isDone()) {
      ctxt.document = this.currentCandidate();

      // collect extent arrays
      ExtentArray pos[] = new ExtentArray[unigramIters.length];
      for (int i = 0; i < unigramIters.length; i++) {
        ExtentIterator unigramIter = unigramIters[i];
        term_cf[i] += unigramIter.count(ctxt);
        pos[i] = unigramIter.extents(ctxt);
      }

      for (int i = 0; i < unigramIters.length - 1; i++) {
        ExtentArray left = pos[i];
        ExtentArray right = pos[i+1];
        od_cf[i] += orderedWindow(left, right);
      }
      this.movePast(ctxt.document);
    }

    for (int i = 0; i < unigramIters.length; i++) {
      double cfv = Math.max(0.5, term_cf[i]);
      term_bg[i] = cfv / collectionLength;
    }

    // calculate statistics for each bigram, note that bigrams are referred to in a this - previous way.
    for (int i = 0; i < unigramIters.length - 1; i++) {
      double od_cfv = Math.max(0.5, od_cf[i]);
      od_bg[i] = od_cfv / Math.max(0.5, term_cf[i]);
    }

    // reset so we can re-use them
    this.reset();
  }

  @Override
  public double score(ScoringContext c) {
    long length = lengthsIter.length(c);

    // collect extent arrays
    for (int i = 0; i < unigramIters.length; i++) {
      ExtentIterator unigramIter = unigramIters[i];
      long term_tf = unigramIter.count(c);
      if(term_tf > 0) {
        pos[i] = unigramIter.extents(c);
      } else {
        pos[i] = ExtentArray.EMPTY;
      }
    }
    double sum = 0.0;
    // add in q_1
    sum += logDirichlet(unigramIters[0].count(c), length, unigramMu, term_bg[0]);

    for (int i=1; i<unigramIters.length; ++i) {
      ExtentArray left = pos[i-1];
      ExtentArray right = pos[i];
      long od_tf = orderedWindow(left, right);
      long unigram_left = unigramIters[i-1].count(c);
      sum +=  logDirichlet(od_tf, unigram_left, bigramMu, od_bg[i-1]);
    }

    return sum;
  }

  @Override
  public double maximumScore() {
    return Double.POSITIVE_INFINITY;
  }

  @Override
  public double minimumScore() {
    return Double.NEGATIVE_INFINITY;
  }

  @Override
  public String getValueString(ScoringContext sc) throws IOException {
    return null;
  }

  @Override
  public AnnotatedNode getAnnotatedNode(ScoringContext sc) throws IOException {
    return null;
  }
}
