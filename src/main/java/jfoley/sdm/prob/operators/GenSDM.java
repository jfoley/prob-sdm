package jfoley.sdm.prob.operators;

import org.lemurproject.galago.core.retrieval.RequiredParameters;
import org.lemurproject.galago.core.retrieval.RequiredStatistics;
import org.lemurproject.galago.core.retrieval.iterator.*;
import org.lemurproject.galago.core.retrieval.processing.ScoringContext;
import org.lemurproject.galago.core.retrieval.query.AnnotatedNode;
import org.lemurproject.galago.core.retrieval.query.NodeParameters;
import org.lemurproject.galago.core.util.ExtentArray;
import org.lemurproject.galago.utility.MathUtils;

import java.io.IOException;

import static jfoley.sdm.prob.operators.FeatureUtil.logDirichlet;
import static jfoley.sdm.prob.operators.FeatureUtil.orderedWindow;
import static jfoley.sdm.prob.operators.FeatureUtil.unorderedWindow;

/**
 * @author jfoley.
 */
@RequiredStatistics(statistics = {"collectionLength"})
@RequiredParameters(parameters = {"unimu", "odmu", "uwmu", "uniw", "odw", "uww"})
public class GenSDM extends DisjunctionIterator implements ScoreIterator {
  private final LengthsIterator lengthsIter;
  private final ExtentIterator[] unigramIters;

  private final double unigramMu;
  private final double bigramMu;
  private final double ubigramMu;
  private final double unigramWeight;
  private final double bigramWeight;
  private final double ubigramWeight;

  double term_bg[], od_bg[], uw_bg[];
  private ExtentArray[] pos;

  public GenSDM(NodeParameters parameters, LengthsIterator lengthsIter, ExtentIterator[] unigramIters) throws IOException {
    super(unigramIters);
    this.lengthsIter = lengthsIter;
    this.unigramIters = unigramIters;
    assert(unigramIters.length > 1);

    double collectionLength = parameters.getLong("collectionLength");

    this.unigramMu = parameters.getDouble("unimu");
    this.bigramMu = parameters.getDouble("odmu");
    this.ubigramMu = parameters.getDouble("uwmu");

    double NumUnigrams = unigramIters.length;
    double NumTwograms = NumUnigrams - 1;

    unigramWeight = parameters.get("uniw", 0.8) / NumUnigrams;
    bigramWeight = parameters.get("odw", 0.15) / NumTwograms;
    ubigramWeight = parameters.get("uww", 0.05) / NumTwograms;

    long startStats = System.currentTimeMillis();
    calculateStats(collectionLength);
    long endStats = System.currentTimeMillis();

    System.out.println("Stats: "+(endStats - startStats));

    this.pos = new ExtentArray[unigramIters.length];
  }

  private void calculateStats(double collectionLength) throws IOException {
    ScoringContext ctxt = new ScoringContext();

    long term_cf[], od_cf[], uw_cf[];

    term_cf = new long[unigramIters.length];
    od_cf = new long[unigramIters.length-1];
    uw_cf = new long[unigramIters.length-1];

    term_bg = new double[unigramIters.length];
    od_bg = new double[unigramIters.length-1];
    uw_bg = new double[unigramIters.length-1];

    // note that 'this' is a disjunction iterator
    while(!this.isDone()) {
      ctxt.document = this.currentCandidate();

      // collect extent arrays
      ExtentArray pos[] = new ExtentArray[unigramIters.length];
      for (int i = 0; i < unigramIters.length; i++) {
        ExtentIterator unigramIter = unigramIters[i];
        term_cf[i] += unigramIter.count(ctxt);
        pos[i] = unigramIter.extents(ctxt);
      }

      for (int i = 0; i < unigramIters.length - 1; i++) {
        ExtentArray left = pos[i];
        ExtentArray right = pos[i+1];
        od_cf[i] += orderedWindow(left, right);
        uw_cf[i] += unorderedWindow(left, right, 8);
      }
      this.movePast(ctxt.document);
    }

    for (int i = 0; i < unigramIters.length; i++) {
      double cfv = Math.max(0.5, term_cf[i]);
      term_bg[i] = cfv / collectionLength;
    }
    for (int i = 0; i < unigramIters.length - 1; i++) {
      double od_cfv = Math.max(0.5, od_cf[i]);
      od_bg[i] = od_cfv / collectionLength;

      double uw_cfv = Math.max(0.5, uw_cf[i]);
      uw_bg[i] = uw_cfv / collectionLength;
    }

    // reset so we can re-use them
    this.reset();
  }

  @Override
  public double score(ScoringContext c) {
    long length = lengthsIter.length(c);

    double unigram = 0.0;
    // collect extent arrays
    for (int i = 0; i < unigramIters.length; i++) {
      ExtentIterator unigramIter = unigramIters[i];
      long term_tf = unigramIter.count(c);

      if(term_tf > 0) {
        pos[i] = unigramIter.extents(c);
      } else {
        pos[i] = ExtentArray.EMPTY;
      }

      unigram += logDirichlet(term_tf, length, unigramMu, term_bg[i]);
    }

    double bigram = 0.0;
    double ubigram = 0.0;

    for (int i = 0; i < unigramIters.length - 1; i++) {
      ExtentArray left = pos[i];
      ExtentArray right = pos[i+1];
      long od_tf = orderedWindow(left, right);
      long uw_tf = unorderedWindow(left, right, 8);

      bigram += logDirichlet(od_tf, length, bigramMu, od_bg[i]);
      ubigram += logDirichlet(uw_tf, length, ubigramMu, uw_bg[i]);
    }

    return MathUtils.weightedLogSumExp(
        new double[]{unigramWeight, bigramWeight, ubigramWeight},
        new double[]{
            unigram, bigram, ubigram
        });
  }

  @Override
  public double maximumScore() {
    return Double.POSITIVE_INFINITY;
  }

  @Override
  public double minimumScore() {
    return Double.NEGATIVE_INFINITY;
  }

  @Override
  public String getValueString(ScoringContext sc) throws IOException {
    return null;
  }

  @Override
  public AnnotatedNode getAnnotatedNode(ScoringContext sc) throws IOException {
    return null;
  }
}
