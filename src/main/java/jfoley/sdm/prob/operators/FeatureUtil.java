package jfoley.sdm.prob.operators;

import org.lemurproject.galago.core.retrieval.iterator.ExtentArrayIterator;
import org.lemurproject.galago.core.util.ExtentArray;

/**
 * @author jfoley
 */
public class FeatureUtil {
  public static long unorderedWindow(ExtentArray left, ExtentArray right, final int width) {
    ExtentArrayIterator iterA = new ExtentArrayIterator(left);
    ExtentArrayIterator iterB = new ExtentArrayIterator(right);

    if(iterA.isDone() || iterB.isDone()) {
      return 0;
    }

    long count = 0;
    boolean hasNext = true;
    while(hasNext) {
      // choose minimum iterator based on start
      final ExtentArrayIterator minIter = (iterA.currentBegin() < iterB.currentBegin()) ? iterA : iterB;
      final int minimumPosition = minIter.currentBegin();
      final int maximumPosition = Math.max(iterA.currentEnd(), iterB.currentEnd());

      // check for a match
      if(maximumPosition - minimumPosition <= width) {
        //extentCache.add(minimumPosition, maximumPosition);
        count++;
      }

      // move minimum iterator
      hasNext = minIter.next();
    }
    return count;
  }

  public static long orderedWindow(ExtentArray leftTerm, ExtentArray rightTerm) {
    final ExtentArrayIterator left = new ExtentArrayIterator(leftTerm);
    final ExtentArrayIterator right = new ExtentArrayIterator(rightTerm);

    // redundant?
    if(left.isDone() || right.isDone())
      return 0;

    long count = 0;
    boolean hasNext = true;
    while(hasNext) {
      final int lhs = left.currentEnd();
      final int rhs = right.currentBegin();

      if(lhs < rhs) {
        hasNext = left.next();
      } else if(lhs > rhs) {
        hasNext = right.next();
      } else { // equal; matched
        count++;
        hasNext = left.next();
      }
    }
    return count;
  }

  public static double logDirichlet(double count, double length, double mu, double background) {
    double numerator = count + mu * background;
    double denominator = length + mu;
    return Math.log(numerator / denominator);
  }
}
