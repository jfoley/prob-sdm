package jfoley.sdm.prob;

import ciir.jfoley.chai.collections.chained.ChaiIterable;
import ciir.jfoley.chai.lang.LazyPtr;

import java.util.*;

/**
 * As used by Sam, Van, and Laura.
 * @author jfoley
 */
public class RobustSplits {
  private static final List<List<Integer>> rob04Splits = Arrays.<List<Integer>>asList(
      Arrays.<Integer>asList(309, 394, 370, 670, 656, 679, 608, 406, 665, 426, 383, 641, 700, 378, 619, 607, 601, 336, 617, 392, 331, 692, 357, 414, 433, 323, 448, 635, 316, 646, 647, 302, 341, 642, 612, 690, 356, 373, 411, 381, 447, 303, 410, 662, 317, 654, 319, 428, 669, 684),
      Arrays.<Integer>asList(632, 397, 322, 432, 695, 308, 663, 627, 328, 624, 683, 360, 678, 674, 349, 348, 371, 419, 651, 675, 327, 440, 312, 365, 424, 369, 343, 652, 604, 698, 352, 602, 434, 390, 374, 422, 689, 403, 611, 423, 364, 643, 446, 338, 688, 301, 638, 386, 680, 623),
      Arrays.<Integer>asList(427, 354, 313, 445, 307, 628, 363, 693, 666, 358, 603, 444, 437, 644, 376, 661, 380, 626, 631, 436, 671, 450, 439, 361, 413, 382, 417, 449, 396, 306, 404, 622, 620, 326, 614, 687, 321, 648, 415, 334, 351, 685, 677, 324, 606, 362, 347, 664, 605, 637),
      Arrays.<Integer>asList(686, 402, 342, 398, 325, 393, 438, 667, 650, 335, 344, 649, 320, 431, 330, 676, 636, 616, 673, 435, 630, 430, 408, 407, 425, 639, 697, 350, 691, 368, 633, 625, 337, 657, 387, 668, 377, 405, 355, 332, 379, 421, 659, 653, 420, 655, 618, 412, 672, 682),
      Arrays.<Integer>asList(353, 329, 315, 613, 694, 681, 345, 395, 699, 416, 314, 658, 634, 640, 375, 621, 388, 367, 391, 318, 441, 346, 305, 401, 442, 645, 310, 311, 696, 629, 372, 304, 409, 609, 429, 384, 399, 366, 339, 418, 385, 610, 333, 443, 359, 615, 400, 340, 660, 389)
  );

  public static class TrainTestSplit {
    public final List<String> trainingQs;
    public final List<String> testingQs;
    private final int index;

    TrainTestSplit(int index, List<String> trainingQs, List<String> testingQs) {
      this.index = index;
      this.trainingQs = trainingQs;
      this.testingQs = testingQs;
    }
    /**
     * @return An id that can be used to differentiate between cv runs.
     */
    public String id() {
      return "robust.cv"+index;
    }

    public <QUERY_T> Map<String, QUERY_T> getTrainingQueries(Map<String, QUERY_T> queries) {
      Map<String, QUERY_T> output = new HashMap<>(trainingQs.size());
      for (String trainingQ : trainingQs) {
        QUERY_T found = queries.get(trainingQ);
        if(found != null) {
          output.put(trainingQ, found);
        } else {
          System.err.println("# Warning: couldn't find qid="+trainingQ);
        }
      }
      return output;
    }

    public <QUERY_T> Map<String, QUERY_T> getTestingQueries(Map<String, QUERY_T> queries) {
      Map<String, QUERY_T> output = new HashMap<>(trainingQs.size());
      for (String testingQ : testingQs) {
        QUERY_T found = queries.get(testingQ);
        if(found != null) {
          output.put(testingQ, found);
        } else {
          System.err.println("# Warning: couldn't find qid="+testingQ);
        }
      }
      return output;
    }
  }

  public static LazyPtr<List<TrainTestSplit>> qids = new LazyPtr<>(() -> {
    List<TrainTestSplit> splits = new ArrayList<>();

    for (int i = 0; i < rob04Splits.size(); i++) {
      List<Integer> testing = rob04Splits.get(i);
      List<Integer> training = new ArrayList<>();
      for (int j = 0; j < rob04Splits.size(); j++) {
        if(j != i) {
          training.addAll(rob04Splits.get(j));
        }
      }

      List<String> test = ChaiIterable.create(testing).map((x) -> Integer.toString(x)).intoList();
      List<String> train = ChaiIterable.create(training).map((x) -> Integer.toString(x)).intoList();
      splits.add(new TrainTestSplit(i, train, test));
    }

    return splits;
  });
  public static int NumSplits() { return qids.get().size(); }
  public static Set<String> CVIds() {
    HashSet<String> ids = new HashSet<>();
    for (TrainTestSplit trainTestSplit : qids.get()) {
      ids.add(trainTestSplit.id());
    }
    return ids;
  }
}
