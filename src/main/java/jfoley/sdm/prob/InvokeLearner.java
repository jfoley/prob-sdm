package jfoley.sdm.prob;

import ciir.jfoley.chai.errors.NotHandledNow;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.string.StrUtil;
import jfoley.sdm.prob.operators.NGramSDM;
import org.lemurproject.galago.contrib.learning.Learner;
import org.lemurproject.galago.contrib.learning.LearnerFactory;
import org.lemurproject.galago.contrib.learning.RetrievalModelInstance;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.utility.Parameters;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author jfoley
 */
public class InvokeLearner {
  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);
    IterUtils.addToParameters(argp, "nsdm", NGramSDM.class);

    String method = argp.get("method", "nsdm");
    switch (method) {
      case "sdm":
      case "nsdm":
        break;
      default: throw new NotHandledNow("method", method);
    }

    LocalRetrieval robust = new LocalRetrieval(argp.get("robust", "/mnt/scratch/jfoley/robust04.galago"), argp);
    TagTokenizer tok = new TagTokenizer();
    Parameters lparam = Parameters.create();

    ArrayList<Parameters> queries = new ArrayList<>();
    for (String line : LinesIterable.fromFile(argp.get("queries", "prob-sdm/data/rob04.titles.tsv"))) {
      String[] col = line.split("\t");

      Parameters q = Parameters.create();
      q.put("number", col[0]);
      String tmpMethod = method;
      List<String> terms = tok.tokenize(col[1]).terms;
      if(terms.size() == 1) {
        tmpMethod = "combine";
      }
      q.put("text", String.format("#%s( %s )", tmpMethod, StrUtil.join(terms, " ")));
      queries.add(q);
    }

    lparam.put("queries", queries);
    /*try (PrintWriter out = IO.openPrintWriter("prob-sdm/data/robust04q.json")) {
      out.println(lparam.toString());
    }*/

    lparam.put("learner", "xfold");
    lparam.put("xfolds", 5L);
    lparam.put("xfoldLearner", "grid");
    lparam.put("gridSize", 10);
    lparam.put("qrels", argp.get("qrels", "prob-sdm/data/robust04.qrels"));
    List<Parameters> sdmParams = new ArrayList<>();
    sdmParams.add(Parameters.parseArray("name", "uniw", "max", 1.0, "min", 0.0));
    sdmParams.add(Parameters.parseArray("name", "odw", "max", 1.0, "min", 0.0));
    sdmParams.add(Parameters.parseArray("name", "uww", "max", 1.0, "min", 0.0));
    lparam.put("learnableParameters", sdmParams);

    Parameters normalRule = Parameters.create();
    normalRule.set("mode", "sum");
    normalRule.set("params", Arrays.asList(new String[]{"0", "1"}));
    normalRule.set("value", 1D);
    lparam.set("normalization", Collections.singletonList(normalRule));

    Learner learner = LearnerFactory.instance(lparam, robust);
    assert learner != null;
    RetrievalModelInstance learn = learner.learn();
    System.out.println(learn.toPrettyString());

    try (PrintWriter out = IO.openPrintWriter(argp.getString("output"))) {
      out.println(learn.toPrettyString());
    }

  }
}
