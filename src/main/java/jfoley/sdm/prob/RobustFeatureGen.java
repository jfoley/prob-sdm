package jfoley.sdm.prob;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.errors.NotHandledNow;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.random.Sample;
import org.lemurproject.galago.core.eval.QueryJudgments;
import org.lemurproject.galago.core.eval.QuerySetJudgments;
import org.lemurproject.galago.core.index.disk.DiskIndex;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.DataIterator;
import org.lemurproject.galago.core.retrieval.processing.ScoringContext;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;

import java.io.PrintWriter;
import java.util.*;

/**
 * @author jfoley
 */
public class RobustFeatureGen {
  enum QType {
    UNIGRAMS,
    BIGRAMS,
    SKIPGRAMS,
  }

  /** Wrapper around galago that let's us do additional transforms if need be. */
  public static Results transformAndExecuteQuery(LocalRetrieval ret, QType type, Node query, Parameters p) throws Exception {
    return ret.transformAndExecuteQuery(query, p);
  }

  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);
    System.err.println(argp.toPrettyString());

    String smoothing = argp.getString("scorer");
    String outputMRF; String outputGen;
    switch (smoothing) {
      case "dirichlet":
        if (!argp.isDouble("mu")) throw new IllegalArgumentException("Must specify mu for dirichlet.");
        outputGen = String.format("gen.dirichlet.mu%d.ranksvm", (int) argp.getDouble("mu"));
        outputMRF = String.format("mrf.dirichlet.mu%d.ranksvm", (int) argp.getDouble("mu"));
        break;
      case "jm":
        if (!argp.isDouble("lambda")) throw new IllegalArgumentException("Must specify lambda for dirichlet.");
        outputGen = String.format("gen.jm.lambda%.3f.ranksvm", argp.getDouble("lambda"));
        outputMRF = String.format("mrf.jm.lambda%.3f.ranksvm", argp.getDouble("lambda"));
        break;
      default: throw new NotHandledNow("scorer", smoothing);
    }
    LocalRetrieval robust = new LocalRetrieval(argp.get("robust", "/mnt/scratch/jfoley/robust04.galago"), argp);
    //LocalRetrieval robust = new LocalRetrieval(argp.get("robust", "/media/jfoley/flash/robust04.galago"), argp);
    TagTokenizer tok = new TagTokenizer();

    List<String> allRobustNames = new ArrayList<>();
    DiskIndex index = (DiskIndex) robust.getIndex();
    ScoringContext ctx = new ScoringContext();
    DataIterator<String> iter = index.getNamesIterator();
    while(!iter.isDone()) {
      long doc = iter.currentCandidate();
      ctx.document = doc;
      String name = iter.data(ctx);
      allRobustNames.add(name);
      iter.movePast(doc);
    }

    System.err.println("Loaded allRobustNames: "+allRobustNames.size());

    Map<String, QueryJudgments> qrels = QuerySetJudgments.loadJudgments(argp.get("qrels", "prob-sdm/data/robust04.qrels"), true, true);

    int depth = (int) argp.get("requested", 1000);

    try (PrintWriter outGen = IO.openPrintWriter(outputGen);
         PrintWriter outMRF = IO.openPrintWriter(outputMRF)) {
      for (String qcol : LinesIterable.fromFile(argp.get("queries", "prob-sdm/data/rob04.titles.tsv"))) {
        String[] col = qcol.split("\t");
        String qid = col[0];
        String text = col[1];
        List<String> terms = tok.tokenize(text).terms;

        QueryJudgments queryJudgments = qrels.get(qid);
        assert(queryJudgments != null);

        System.err.println(Parameters.parseArray("qid", qid, "text", text, "terms", terms));

        // generate unigram feature
        Node unigram = new Node("combine");
        for (String term : terms) {
          unigram.addChild(Node.Text(term));
        }

        // generate bigram features
        Node bigrams = new Node("combine");
        Node ubigrams = new Node("combine");
        for (List<String> bigram : ListFns.sliding(terms, 2)) {

          // ordered bigrams
          Node od1 = new Node("od");
          od1.getNodeParameters().set("default", 1);
          od1.addChild(Node.Text(bigram.get(0)));
          od1.addChild(Node.Text(bigram.get(1)));
          bigrams.addChild(od1);

          // unordered bigrams
          Node uw8 = new Node("uw");
          uw8.getNodeParameters().set("default", 8);
          uw8.addChild(Node.Text(bigram.get(0)));
          uw8.addChild(Node.Text(bigram.get(1)));
          ubigrams.addChild(uw8);
        }

        // grab a random sample from collection to not bias toward good candidates.
        Set<String> workingSet = new HashSet<>();
        workingSet.addAll(Sample.byRandomWeight(allRobustNames, 3000));

        Map<String, Double> qls;
        Map<String, Double> ods;
        Map<String, Double> uws;
        if(terms.size() > 1) {
          Parameters qp = argp.clone();
          qp.put("requested", depth);

          Results qlr = transformAndExecuteQuery(robust, QType.UNIGRAMS, unigram, qp.clone());
          Results odr = transformAndExecuteQuery(robust, QType.BIGRAMS, bigrams, qp.clone());
          Results uwr = transformAndExecuteQuery(robust, QType.SKIPGRAMS, ubigrams, qp.clone());

          workingSet.addAll(qlr.resultSet());
          workingSet.addAll(odr.resultSet());
          workingSet.addAll(uwr.resultSet());

          System.err.println("# qid=" + qid + " pool=" + workingSet.size());
          qp = argp.clone();
          qp.put("working", new ArrayList<>(workingSet));
          qp.put("requested", workingSet.size());

          qls = byDocumentFeatures(transformAndExecuteQuery(robust, QType.UNIGRAMS, unigram, qp.clone()));
          ods = byDocumentFeatures(transformAndExecuteQuery(robust, QType.BIGRAMS, bigrams, qp.clone()));
          uws = byDocumentFeatures(transformAndExecuteQuery(robust, QType.SKIPGRAMS, ubigrams, qp.clone()));
        } else { // unigrams + zeros
          // pool + random sample
          workingSet.addAll(robust.transformAndExecuteQuery(unigram, Parameters.parseArray("requested", depth)).resultSet());

          System.err.println("# qid=" + qid + " pool=" + workingSet.size());
          Parameters qp = argp.clone();
          qp.put("working", new ArrayList<>(workingSet));
          qp.put("requested", workingSet.size());

          Results justUnigrams = transformAndExecuteQuery(robust, QType.UNIGRAMS, unigram, qp.clone());
          workingSet = justUnigrams.resultSet();
          qls = byDocumentFeatures(justUnigrams);
          ods = Collections.emptyMap();
          uws = Collections.emptyMap();
        }

        // mrf output sum ( lambda log (feature) )
        for (String doc : workingSet) {
          StringBuilder features = new StringBuilder();
          if(queryJudgments.isRelevant(doc)) {
            features.append("1 ");
          } else {
            features.append("0 ");
          }
          features.append("qid:").append(qid).append(' ');

          double unigramScore = qls.get(doc);
          features.append("1:").append(unigramScore).append(' ');
          if(terms.size() > 1) {
            features.append("2:").append(ods.get(doc)).append(' ');
            features.append("3:").append(uws.get(doc)).append(' ');
          } else {
            features.append("2:").append(unigramScore).append(' ');
            features.append("3:").append(unigramScore).append(' ');
          }

          features.append("# ").append(doc);
          outMRF.println(features.toString());
        }

        // generative output: sum ( lambda exp(log(feature)) )
        for (String doc : workingSet) {
          StringBuilder features = new StringBuilder();
          if(queryJudgments.isRelevant(doc)) {
            features.append("1 ");
          } else {
            features.append("0 ");
          }
          features.append("qid:").append(qid).append(' ');

          double unigramScore = Math.exp(qls.get(doc));
          features.append("1:").append(unigramScore).append(' ');
          if(terms.size() > 1) {
            features.append("2:").append(Math.exp(ods.get(doc))).append(' ');
            features.append("3:").append(Math.exp(uws.get(doc))).append(' ');
          } else {
            features.append("2:").append(unigramScore).append(' ');
            features.append("3:").append(unigramScore).append(' ');
          }

          features.append("# ").append(doc);
          outGen.println(features.toString());
        }
      } // end for each query
    } // end I/O

  }


  public static Map<String,Double> byDocumentFeatures(Results galagoResults) {
    HashMap<String, Double> scores = new HashMap<>(galagoResults.scoredDocuments.size());
    for (ScoredDocument sdoc : galagoResults.scoredDocuments) {
      scores.put(sdoc.documentName, sdoc.score);
    }
    return scores;
  }

}
