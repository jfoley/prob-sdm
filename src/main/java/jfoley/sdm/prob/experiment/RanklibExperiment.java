package jfoley.sdm.prob.experiment;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.errors.NotHandledNow;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.random.Sample;
import jfoley.sdm.prob.IterUtils;
import jfoley.sdm.prob.RobustFeatureGen;
import jfoley.sdm.prob.RobustSplits;
import jfoley.sdm.prob.operators.NGramBigram;
import jfoley.sdm.prob.operators.NGramUBigram;
import org.lemurproject.galago.core.eval.QueryJudgments;
import org.lemurproject.galago.core.eval.QuerySetJudgments;
import org.lemurproject.galago.core.index.disk.DiskIndex;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.iterator.DataIterator;
import org.lemurproject.galago.core.retrieval.processing.ScoringContext;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;

import java.io.PrintWriter;
import java.util.*;

/**
 * @author jfoley
 */
public class RanklibExperiment {
  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);

    // bigram components of ngram-sdm
    IterUtils.addToParameters(argp, "nbigram", NGramBigram.class);
    IterUtils.addToParameters(argp, "nubigram", NGramUBigram.class);

    // Make sure all the data files are available...
    LocalRetrieval robust = new LocalRetrieval(argp.getString("robust"), argp);
    TagTokenizer tok = new TagTokenizer();
    Map<String, QueryJudgments> qrels = QuerySetJudgments.loadJudgments(argp.getString("qrels"), true, true);
    RobustSplits.TrainTestSplit split = RobustSplits.qids.get().get((int) argp.getLong("split"));
    assert(split != null);


    // Make sure all the learning parameters are available before we get too far into the job.
    boolean train = argp.getBoolean("train");
    boolean generative = argp.getBoolean("generative");

    // Make sure I gave all the parameters I needed.
    String method = argp.getString("method"); // sdm, nsdm
    switch (method) {
      case "msdm": break;
      case "nsdm": break;
      default: throw new NotHandledNow("method", method);
    }

    List<String> allRobustNames = new ArrayList<>();
    DiskIndex index = (DiskIndex) robust.getIndex();
    ScoringContext ctx = new ScoringContext();
    DataIterator<String> iter = index.getNamesIterator();
    while(!iter.isDone()) {
      long doc = iter.currentCandidate();
      ctx.document = doc;
      String name = iter.data(ctx);
      allRobustNames.add(name);
      iter.movePast(doc);
    }

    System.err.println("Loaded allRobustNames: "+allRobustNames.size());

    Map<String, List<String>> queries = new HashMap<>();
    for (String line : LinesIterable.fromFile(argp.get("queries", "prob-sdm/data/rob04.titles.tsv"))) {
      String[] col = line.split("\t");
      List<String> terms = tok.tokenize(col[1]).terms;
      queries.put(col[0], terms);
    }

    Map<String,List<String>> activeQs = train ? split.getTrainingQueries(queries) : split.getTestingQueries(queries);
    Map<String, QueryJudgments> activeQrels = train ? split.getTrainingQueries(qrels) : split.getTestingQueries(qrels);

    try (PrintWriter output = IO.openPrintWriter(argp.getString("output"))) {
      for (Map.Entry<String, List<String>> kv : activeQs.entrySet()) {
        String qid = kv.getKey();
        List<String> terms = kv.getValue();
        QueryJudgments queryJudgments = activeQrels.get(qid);
        assert(queryJudgments != null);

        List<Node> features = new ArrayList<>();

        if (terms.size() == 1) {
          Node unigram = new Node("combine");
          unigram.addChild(Node.Text(terms.get(0)));
          features.add(unigram.clone());
          features.add(unigram.clone());
          features.add(unigram.clone());
        } else {
          Node unigram = new Node("combine");
          Node bigram = new Node("combine");
          Node ubigram = new Node("combine");

          for (String term : terms) {
            unigram.addChild(Node.Text(term));
          }
          for (List<String> bigrams : ListFns.sliding(terms, 2)) {
            String lhs = bigrams.get(0);
            String rhs = bigrams.get(1);
            boolean mrf = method.equals("msdm");

            Node od1 = new Node(mrf ? "bigram" : "nbigram");
            od1.getNodeParameters().set("default", 1);
            od1.addChild(Node.Text(lhs));
            od1.addChild(Node.Text(rhs));
            bigram.addChild(od1);

            Node uw8 = new Node(mrf ? "ubigram" : "nubigram");
            uw8.getNodeParameters().set("default", 8);
            uw8.addChild(Node.Text(lhs));
            uw8.addChild(Node.Text(rhs));
            ubigram.addChild(uw8);
          }

          features.add(unigram);
          features.add(bigram);
          features.add(ubigram);
        }
        assert (features.get(0).numChildren() > 0);
        assert (features.get(1).numChildren() > 0);
        assert (features.get(2).numChildren() > 0);

        // grab a random sample from collection to not bias toward good candidates.
        Set<String> workingSet = new HashSet<>();
        workingSet.addAll(Sample.byRandomWeight(allRobustNames, 3000));

        // pool results from the features:
        for (int i = 0; i < features.size(); i++) {
          Parameters qp = argp.clone();
          System.err.println("# qid=" + qid + " feature[" + i + "]=" + features.get(i));
          Results res = robust.transformAndExecuteQuery(features.get(i), qp);
          workingSet.addAll(res.resultSet());
        }
        System.err.println("# qid=" + qid + " pool=" + workingSet.size());

        List<Map<String, Double>> featureScores = new ArrayList<>();
        for (Node node : features) {
          Parameters qp = argp.clone();
          qp.put("working", new ArrayList<>(workingSet));
          qp.put("requested", workingSet.size());
          Results res = robust.transformAndExecuteQuery(node, qp);
          featureScores.add(RobustFeatureGen.byDocumentFeatures(res));
        }

        // write featureScores:
        for (String doc : workingSet) {
          StringBuilder ranklibStr = new StringBuilder();
          if(queryJudgments.isRelevant(doc)) {
            ranklibStr.append("1 ");
          } else {
            ranklibStr.append("0 ");
          }
          ranklibStr.append("qid:").append(qid).append(' ');

          for (int i = 0; i < features.size(); i++) {
            double score = featureScores.get(i).get(doc);
            if(generative) {
              score = Math.exp(score);
            }
            ranklibStr.append(i+1).append(":").append(score).append(' ');
          }

          ranklibStr.append("# ").append(doc);
          output.println(ranklibStr.toString());
        }
      }
    }

  }
}

