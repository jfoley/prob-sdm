package jfoley.sdm.prob.experiment;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.errors.NotHandledNow;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.string.StrUtil;
import jfoley.sdm.prob.IterUtils;
import jfoley.sdm.prob.RobustSplits;
import jfoley.sdm.prob.operators.*;
import org.lemurproject.galago.core.eval.EvalDoc;
import org.lemurproject.galago.core.eval.QueryJudgments;
import org.lemurproject.galago.core.eval.QuerySetJudgments;
import org.lemurproject.galago.core.eval.QuerySetResults;
import org.lemurproject.galago.core.eval.aggregate.QuerySetEvaluator;
import org.lemurproject.galago.core.eval.aggregate.QuerySetEvaluatorFactory;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Grid tuning, take two, with separate mus.
 * @author jfoley
 */
public class ActualPaperExperiment {
  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);
    // bigram components of ngram-sdm
    IterUtils.addToParameters(argp, "nbigram", NGramBigram.class);
    IterUtils.addToParameters(argp, "nubigram", NGramUBigram.class);
    // ngram-sdm
    IterUtils.addToParameters(argp, "nsdm", NGramSDM.class);
    // ngram-bag-sdm (simpler log-likelihood)
    IterUtils.addToParameters(argp, "nbsdm", NGramBagSDM.class);
    // mrf-sdm a.k.a. sdm
    IterUtils.addToParameters(argp, "msdm", MRFSDM.class);
    // gen-sdm
    IterUtils.addToParameters(argp, "gsdm", GenSDM.class);

    QuerySetEvaluator map = QuerySetEvaluatorFactory.create("map", argp);
    assert(map != null);

    // Make sure all the data files are available...
    LocalRetrieval robust = new LocalRetrieval(argp.getString("robust"), argp);
    TagTokenizer tok = new TagTokenizer();
    Map<String, QueryJudgments> qrels = QuerySetJudgments.loadJudgments(argp.getString("qrels"), true, true);
    RobustSplits.TrainTestSplit split = RobustSplits.qids.get().get((int) argp.getLong("split"));
    assert(split != null);

    // Make sure all the learning parameters are available before we get too far into the job.
    boolean train = argp.getBoolean("train");

    // Make sure I gave all the parameters I needed.
    String method = argp.getString("method"); // sdm, nsdm
    switch (method) {
      case "combine":
        assert(argp.isDouble("mu"));
        break;
      case "nbigram":
        assert(argp.isDouble("unimu"));
        assert(argp.isDouble("odmu"));
        break;
      case "nubigram":
        assert(argp.isDouble("unimu"));
        assert(argp.isDouble("uwmu"));
        break;
      case "bigram":
      case "ubigram":
        assert(argp.isDouble("mu"));
        break;
      case "gsdm":
      case "msdm":
        // weights
        assert(argp.isDouble("uniw"));
        assert(argp.isDouble("odw"));
        assert(argp.isDouble("uww"));
        // mus
        assert(argp.isDouble("unimu"));
        assert(argp.isDouble("odmu"));
        assert(argp.isDouble("uwmu"));
      case "nsdm":
      case "nbsdm":
        // weights
        assert(argp.isDouble("uniw"));
        assert(argp.isDouble("odw"));
        assert(argp.isDouble("uww"));
        // mus
        assert(argp.isDouble("unimu"));
        assert(argp.isDouble("nodmu")); // specialized
        assert(argp.isDouble("nuwmu")); // specialized
        break;
      default: throw new NotHandledNow("method", method);
    }

    Map<String, Node> queries = new HashMap<>();
    for (String line : LinesIterable.fromFile(argp.get("queries", "prob-sdm/data/rob04.titles.tsv"))) {
      String[] col = line.split("\t");

      Parameters q = Parameters.create();
      q.put("number", col[0]);
      List<String> terms = tok.tokenize(col[1]).terms;
      String gtext;
      if(terms.size() == 1) {
        gtext =  String.format("#combine( %s )", StrUtil.join(terms, " "));
        queries.put(col[0], StructuredQuery.parse(gtext));
        continue;
      }

      // special!
      if (method.equals("bigram") || method.equals("ubigram")) {
        Node outside = new Node("combine");
        for (List<String> lr : ListFns.sliding(terms, 2)) {
          // need to pass widths to bigram and ubigram:
          Node biFeature = new Node(method);
          biFeature.getNodeParameters().set("default", method.equals("bigram") ? 1 : 8);
          for (String term : lr) {
            biFeature.addChild(Node.Text(term));
          }
          outside.addChild(biFeature);
        }
        queries.put(col[0], outside);
        continue;
      }

      // default case:
      gtext =  String.format("#%s( %s )", method, StrUtil.join(terms, " "));
      queries.put(col[0], StructuredQuery.parse(gtext));
    }

    Map<String,Node> activeQs = train ? split.getTrainingQueries(queries) : split.getTestingQueries(queries);
    Map<String, QueryJudgments> activeQrels = train ? split.getTrainingQueries(qrels) : split.getTestingQueries(qrels);

    // To-evaluate.
    Map<String, List<EvalDoc>> results = new HashMap<>();

    try (PrintWriter trecrun = IO.openPrintWriter(argp.getString("output"))) {
      for (Map.Entry<String, Node> kv : activeQs.entrySet()) {
        String qid = kv.getKey();
        Node query = kv.getValue();
        Parameters qp = argp.clone();
        System.err.println("# "+qid+ " " +query.toString());
        Results res = robust.transformAndExecuteQuery(query, qp);
        results.put(qid, ListFns.castView(res.scoredDocuments));
        for (ScoredDocument sdoc : res.scoredDocuments) {
          trecrun.println(sdoc.toTRECformat(qid));
        }
      }
    }

    double MAP = map.evaluate(new QuerySetResults(results), new QuerySetJudgments(activeQrels));
    System.out.println("Mean_Average_Precision: "+MAP);
  }
}
