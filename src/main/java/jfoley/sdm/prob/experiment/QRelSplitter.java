package jfoley.sdm.prob.experiment;

import jfoley.sdm.prob.RobustSplits;
import org.lemurproject.galago.core.eval.QuerySetJudgments;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;

/**
 * @author jfoley
 */
public class QRelSplitter {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    QuerySetJudgments qrels = new QuerySetJudgments(argp.getString("qrels"), true, true);
    RobustSplits.TrainTestSplit split = RobustSplits.qids.get().get((int) argp.getLong("split"));
    assert(split != null);
    boolean train = argp.getBoolean("train");
    QuerySetJudgments activeQrels = new QuerySetJudgments(train ? split.getTrainingQueries(qrels) : split.getTestingQueries(qrels));
    activeQrels.saveJudgments(new File(argp.getString("output")));
  }
}
