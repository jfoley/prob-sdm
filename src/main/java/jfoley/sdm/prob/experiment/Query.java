package jfoley.sdm.prob.experiment;

/**
 * @author jfoley
 */
public class Query {
  public final String qid;
  public final String text;

  public Query(String qid, String text) {
    this.qid = qid;
    this.text = text;
  }
  public int hashCode() {
    return qid.hashCode();
  }
  public boolean equals(Object o) {
    if(o instanceof Query) {
      return qid.equals(((Query) o).qid);
    }
    return false;
  }
}
