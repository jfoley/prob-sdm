package jfoley.sdm.prob.experiment;

import ciir.jfoley.chai.lang.DoubleFns;
import ciir.jfoley.chai.string.StrUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
public class LambdaSettings {
  public static void main(String[] args) {
    List<String> tuples = new ArrayList<>();
    for (int i = 0; i < 20; i++) {
      for (int j = 0; j < 20; j++) {
        double x = i / 20.0;
        double y = j / 20.0;
        double z = 1.0 - x - y;
        if(z <= 0) continue;
        if(DoubleFns.equals(x, 1, 0.01) || DoubleFns.equals(y, 1, 0.01) || DoubleFns.equals(z, 1, 0.01)) continue;
        //if(DoubleFns.equals(x, 0, 0.01) || DoubleFns.equals(y, 0, 0.01) || DoubleFns.equals(z, 0, 0.01)) continue;
        tuples.add(String.format("(%.2f, %.2f, %.2f)", x, y, z));
      }
    }

    System.out.println(tuples.size());
    System.out.println("[\n  " + StrUtil.join(tuples, ",\n  ") +"]");


    // find lambda settings close to good ones:

  }
}
