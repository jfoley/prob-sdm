package jfoley.sdm.prob;

import ciir.umass.edu.eval.Evaluator;

/**
 * @author jfoley
 */
public class InvokeRanklib {
  public static void main(String[] args) {
    Evaluator.main(new String[] {"-metric2t", "map", "-train", "mrf.ranksvm", "-kcv", "5"});
  }
}
