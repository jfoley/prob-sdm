package jfoley.sdm.prob;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.errors.NotHandledNow;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.string.StrUtil;
import jfoley.sdm.prob.operators.MRFSDM;
import jfoley.sdm.prob.operators.NGramSDM;
import org.lemurproject.galago.core.eval.EvalDoc;
import org.lemurproject.galago.core.eval.QueryJudgments;
import org.lemurproject.galago.core.eval.QuerySetJudgments;
import org.lemurproject.galago.core.eval.QuerySetResults;
import org.lemurproject.galago.core.eval.aggregate.QuerySetEvaluator;
import org.lemurproject.galago.core.eval.aggregate.QuerySetEvaluatorFactory;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Grid learnifier.
 * @author jfoley
 */
public class SDMvsNSDMGridLearner {
  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);
    IterUtils.addToParameters(argp, "nsdm", NGramSDM.class);
    IterUtils.addToParameters(argp, "msdm", MRFSDM.class);
    QuerySetEvaluator map = QuerySetEvaluatorFactory.create("map", argp);
    assert(map != null);

    // Make sure all the data files are available...
    LocalRetrieval robust = new LocalRetrieval(argp.getString("robust"), argp);
    TagTokenizer tok = new TagTokenizer();
    Map<String, QueryJudgments> qrels = QuerySetJudgments.loadJudgments(argp.getString("qrels"), true, true);
    RobustSplits.TrainTestSplit split = RobustSplits.qids.get().get((int) argp.getLong("split"));
    assert(split != null);

    // Make sure all the learning parameters are available before we get too far into the job.
    boolean train = argp.getBoolean("train");
    double mu = argp.getDouble("mu");
    double unigramWeight = argp.getDouble("uniw");
    double bigramWeight = argp.getDouble("odw");
    double ubigramWeight = argp.getDouble("uww");

    System.err.println(Parameters.parseArray(
        "mu", mu,
        "uniw", unigramWeight,
        "odw", bigramWeight,
        "uww", ubigramWeight
    ));

    String method = argp.getString("method"); // sdm, nsdm
    switch (method) {
      case "sdm":
      case "nsdm":
        break;
      default: throw new NotHandledNow("method", method);
    }

    Map<String, Node> queries = new HashMap<>();
    for (String line : LinesIterable.fromFile(argp.get("queries", "prob-sdm/data/rob04.titles.tsv"))) {
      String[] col = line.split("\t");

      Parameters q = Parameters.create();
      q.put("number", col[0]);
      String tmpMethod = method;
      List<String> terms = tok.tokenize(col[1]).terms;
      if(terms.size() == 1) {
        tmpMethod = "combine";
      }
      String gtext = String.format("#%s( %s )", tmpMethod, StrUtil.join(terms, " "));
      queries.put(col[0], StructuredQuery.parse(gtext));
    }

    Map<String,Node> activeQs = train ? split.getTrainingQueries(queries) : split.getTestingQueries(queries);
    Map<String, QueryJudgments> activeQrels = train ? split.getTrainingQueries(qrels) : split.getTestingQueries(qrels);

    // To-evaluate.
    Map<String, List<EvalDoc>> results = new HashMap<>();

    try (PrintWriter trecrun = IO.openPrintWriter(argp.getString("output"))) {
      for (Map.Entry<String, Node> kv : activeQs.entrySet()) {
        String qid = kv.getKey();
        Node query = kv.getValue();
        Parameters qp = argp.clone();
        System.err.println("# "+qid);
        Results res = robust.transformAndExecuteQuery(query, qp);
        results.put(qid, ListFns.castView(res.scoredDocuments));
        for (ScoredDocument sdoc : res.scoredDocuments) {
          trecrun.println(sdoc.toTRECformat(qid));
        }
      }
    }

    double MAP = map.evaluate(new QuerySetResults(results), new QuerySetJudgments(activeQrels));
    System.out.println("Mean_Average_Precision: "+MAP);
  }
}
