#!/usr/bin/python2

import socket, sys

host = socket.gethostname()
sys.stderr.write(host+'\n');

boilerplate = """
JAVA:=$(shell echo $(JAVA_HOME)/bin/java)
JOPT:=-ea -Xmx7G
JAR:='target/prob-sdm-0.1.0-SNAPSHOT.jar'
"""
print(boilerplate)

index = "/media/jfoley/flash/robust04.galago"

if host == 'sydney.cs.umass.edu':
    index = "/mnt/nfs/work3/jfoley/indexes/robust04.galago"
    print("PREFIX=qsub -b y -cwd -sync y -l mem_free=8G -l mem_token=8G -o $@.out -e $@.err ")
elif host == 'taree':
    index = "/mnt/scratch/jfoley/robust04.galago"
else:
    print("PREFIX:=")

# default rule
print(".PHONY: default")
print(".DEFAULT: default")
print("default:")
print("\t@echo ${PREFIX} ${JAVA} ${JOPT} -cp ${JAR}")
print

RANKLIB="${PREFIX} ${JAVA} ${JOPT} -cp ${JAR} ciir.umass.edu.eval.Evaluator "

print(".PHONY: ranklib")
print("ranklib:")
print("\t@echo %s" % RANKLIB)
print

queries="data/rob04.titles.tsv"
qrels = "data/robust04.qrels"
gridLearnJob = "${PREFIX} ${JAVA} ${JOPT} -cp ${JAR} jfoley.sdm.prob.experiment.ActualPaperExperiment --qrels=%s --queries=%s --robust=%s" % (qrels, queries, index)
GEN_QRELS = "${PREFIX} ${JAVA} ${JOPT} -cp ${JAR} jfoley.sdm.prob.experiment.QRelSplitter "


unigram_jobs = []
for (tname, training) in [("train", "true"), ("test", "false")]:
#for mu in [10, 250, 500, 750, 1000, 1250, 1500, 1750, 2000, 2250, 2500, 3000, 3500, 4000, 4500, 5000, 10000]:
  for mu in [1000]:
    mu_arg = " --mu=%.1f" % mu
    for fold in [0,1,2,3,4]:
      fold_arg = " --split=%d " % (fold)
      method_arg = " --method=combine"
      output = "unigram.split%d.mu%d.%s.trecrun" % (fold, mu, tname)
      unigram_jobs += [output]
      print(output+":")
      print("\t%s %s %s %s --train=%s --output=$@" % (gridLearnJob, mu_arg, fold_arg, method_arg, training))
      print

# decided by cross-validation on the training data -- not pollution here, all folds agreed on 1000.
unigram_mu = 1000

ngram_bigram_jobs = []
for (method, mu_name) in [("nbigram", "odmu"), ("nubigram", "uwmu") ]:
    for mu in [1, 5, 10, 50, 100, 150, 200, 250, 500, 750, 1000]:
        mu_arg = " --mu=%.1f --unimu=%.1f --%s=%.1f " % (unigram_mu, unigram_mu, mu_name, mu)
        for fold in [0,1,2,3,4]:
            fold_arg = " --split=%d " % fold
            method_arg = " --method=%s" % method
            output = "%s.split%d.%s%d.mu%d.train.trecrun" % (method, fold, mu_name, mu, unigram_mu)
            ngram_bigram_jobs += [output]
            print(output+":")
            print("\t%s %s %s %s --train=true --output=$@" % (gridLearnJob, mu_arg, fold_arg, method_arg))
            print

regular_bigram_jobs = []
for method in ["bigram", "ubigram"]:
    for mu in [10, 250, 500, 750, 1000, 1250, 1500, 1750, 2000, 2250, 2500, 3000, 3500, 4000, 4500, 5000, 10000, 15000, 17500, 18750, 20000, 21250, 22500, 25000, 30000, 35000, 40000, 45000, 50000, 100000]:
        mu_arg = " --mu=%.1f" % (mu)
        for fold in [0,1,2,3,4]:
            fold_arg = " --split=%d " % fold
            method_arg = " --method=%s" % method
            output = "%s.split%d.mu%d.train.trecrun" % (method, fold, mu)
            regular_bigram_jobs += [output]
            print(output+":")
            print("\t%s %s %s %s --train=true --output=$@" % (gridLearnJob, mu_arg, fold_arg, method_arg))
            print

lambda_settings = [
    (0.00, 0.05, 0.95), (0.00, 0.10, 0.90), (0.00, 0.15, 0.85), (0.00, 0.20, 0.80),
    (0.00, 0.25, 0.75), (0.00, 0.30, 0.70), (0.00, 0.35, 0.65), (0.00, 0.40, 0.60),
    (0.00, 0.45, 0.55), (0.00, 0.50, 0.50), (0.00, 0.55, 0.45), (0.00, 0.60, 0.40),
    (0.00, 0.65, 0.35), (0.00, 0.70, 0.30), (0.00, 0.75, 0.25), (0.00, 0.80, 0.20),
    (0.00, 0.85, 0.15), (0.00, 0.90, 0.10), (0.00, 0.95, 0.05), (0.05, 0.00, 0.95),
    (0.05, 0.05, 0.90), (0.05, 0.10, 0.85), (0.05, 0.15, 0.80), (0.05, 0.20, 0.75),
    (0.05, 0.25, 0.70), (0.05, 0.30, 0.65), (0.05, 0.35, 0.60), (0.05, 0.40, 0.55),
    (0.05, 0.45, 0.50), (0.05, 0.50, 0.45), (0.05, 0.55, 0.40), (0.05, 0.60, 0.35),
    (0.05, 0.65, 0.30), (0.05, 0.70, 0.25), (0.05, 0.75, 0.20), (0.05, 0.80, 0.15),
    (0.05, 0.85, 0.10), (0.05, 0.90, 0.05), (0.10, 0.00, 0.90), (0.10, 0.05, 0.85),
    (0.10, 0.10, 0.80), (0.10, 0.15, 0.75), (0.10, 0.20, 0.70), (0.10, 0.25, 0.65),
    (0.10, 0.30, 0.60), (0.10, 0.35, 0.55), (0.10, 0.40, 0.50), (0.10, 0.45, 0.45),
    (0.10, 0.50, 0.40), (0.10, 0.55, 0.35), (0.10, 0.60, 0.30), (0.10, 0.65, 0.25),
    (0.10, 0.70, 0.20), (0.10, 0.75, 0.15), (0.10, 0.80, 0.10), (0.10, 0.85, 0.05),
    (0.15, 0.00, 0.85), (0.15, 0.05, 0.80), (0.15, 0.10, 0.75), (0.15, 0.15, 0.70),
    (0.15, 0.20, 0.65), (0.15, 0.25, 0.60), (0.15, 0.30, 0.55), (0.15, 0.35, 0.50),
    (0.15, 0.40, 0.45), (0.15, 0.45, 0.40), (0.15, 0.50, 0.35), (0.15, 0.55, 0.30),
    (0.15, 0.60, 0.25), (0.15, 0.65, 0.20), (0.15, 0.70, 0.15), (0.15, 0.75, 0.10),
    (0.15, 0.80, 0.05), (0.20, 0.00, 0.80), (0.20, 0.05, 0.75), (0.20, 0.10, 0.70),
    (0.20, 0.15, 0.65), (0.20, 0.20, 0.60), (0.20, 0.25, 0.55), (0.20, 0.30, 0.50),
    (0.20, 0.35, 0.45), (0.20, 0.40, 0.40), (0.20, 0.45, 0.35), (0.20, 0.50, 0.30),
    (0.20, 0.55, 0.25), (0.20, 0.60, 0.20), (0.20, 0.65, 0.15), (0.20, 0.70, 0.10),
    (0.20, 0.75, 0.05), (0.25, 0.00, 0.75), (0.25, 0.05, 0.70), (0.25, 0.10, 0.65),
    (0.25, 0.15, 0.60), (0.25, 0.20, 0.55), (0.25, 0.25, 0.50), (0.25, 0.30, 0.45),
    (0.25, 0.35, 0.40), (0.25, 0.40, 0.35), (0.25, 0.45, 0.30), (0.25, 0.50, 0.25),
    (0.25, 0.55, 0.20), (0.25, 0.60, 0.15), (0.25, 0.65, 0.10), (0.25, 0.70, 0.05),
    (0.30, 0.00, 0.70), (0.30, 0.05, 0.65), (0.30, 0.10, 0.60), (0.30, 0.15, 0.55),
    (0.30, 0.20, 0.50), (0.30, 0.25, 0.45), (0.30, 0.30, 0.40), (0.30, 0.35, 0.35),
    (0.30, 0.40, 0.30), (0.30, 0.45, 0.25), (0.30, 0.50, 0.20), (0.30, 0.55, 0.15),
    (0.30, 0.60, 0.10), (0.30, 0.65, 0.05), (0.35, 0.00, 0.65), (0.35, 0.05, 0.60),
    (0.35, 0.10, 0.55), (0.35, 0.15, 0.50), (0.35, 0.20, 0.45), (0.35, 0.25, 0.40),
    (0.35, 0.30, 0.35), (0.35, 0.35, 0.30), (0.35, 0.40, 0.25), (0.35, 0.45, 0.20),
    (0.35, 0.50, 0.15), (0.35, 0.55, 0.10), (0.35, 0.60, 0.05), (0.40, 0.00, 0.60),
    (0.40, 0.05, 0.55), (0.40, 0.10, 0.50), (0.40, 0.15, 0.45), (0.40, 0.20, 0.40),
    (0.40, 0.25, 0.35), (0.40, 0.30, 0.30), (0.40, 0.35, 0.25), (0.40, 0.40, 0.20),
    (0.40, 0.45, 0.15), (0.40, 0.50, 0.10), (0.40, 0.55, 0.05), (0.45, 0.00, 0.55),
    (0.45, 0.05, 0.50), (0.45, 0.10, 0.45), (0.45, 0.15, 0.40), (0.45, 0.20, 0.35),
    (0.45, 0.25, 0.30), (0.45, 0.30, 0.25), (0.45, 0.35, 0.20), (0.45, 0.40, 0.15),
    (0.45, 0.45, 0.10), (0.45, 0.50, 0.05), (0.50, 0.00, 0.50), (0.50, 0.05, 0.45),
    (0.50, 0.10, 0.40), (0.50, 0.15, 0.35), (0.50, 0.20, 0.30), (0.50, 0.25, 0.25),
    (0.50, 0.30, 0.20), (0.50, 0.35, 0.15), (0.50, 0.40, 0.10), (0.50, 0.45, 0.05),
    (0.55, 0.00, 0.45), (0.55, 0.05, 0.40), (0.55, 0.10, 0.35), (0.55, 0.15, 0.30),
    (0.55, 0.20, 0.25), (0.55, 0.25, 0.20), (0.55, 0.30, 0.15), (0.55, 0.35, 0.10),
    (0.55, 0.40, 0.05), (0.60, 0.00, 0.40), (0.60, 0.05, 0.35), (0.60, 0.10, 0.30),
    (0.60, 0.15, 0.25), (0.60, 0.20, 0.20), (0.60, 0.25, 0.15), (0.60, 0.30, 0.10),
    (0.60, 0.35, 0.05), (0.65, 0.00, 0.35), (0.65, 0.05, 0.30), (0.65, 0.10, 0.25),
    (0.65, 0.15, 0.20), (0.65, 0.20, 0.15), (0.65, 0.25, 0.10), (0.65, 0.30, 0.05),
    (0.70, 0.00, 0.30), (0.70, 0.05, 0.25), (0.70, 0.10, 0.20), (0.70, 0.15, 0.15),
    (0.70, 0.20, 0.10), (0.70, 0.25, 0.05), (0.70, 0.30, 0.00), (0.75, 0.00, 0.25),
    (0.75, 0.05, 0.20), (0.75, 0.10, 0.15), (0.75, 0.15, 0.10), (0.75, 0.20, 0.05),
    (0.80, 0.00, 0.20), (0.80, 0.05, 0.15), (0.80, 0.10, 0.10), (0.80, 0.15, 0.05),
    (0.85, 0.00, 0.15), (0.85, 0.05, 0.10), (0.85, 0.10, 0.05), (0.85, 0.15, 0.00),
    (0.90, 0.00, 0.10), (0.90, 0.05, 0.05), (0.95, 0.00, 0.05), (0.95, 0.05, 0.00)
    ] 

split_settings = [
    (0, 1000, 5, 1, 18750, 20000),
    (1, 1000, 5, 1, 18750,  2500),
    (2, 1000, 5, 1, 18750, 20000),
    (3, 1000, 5, 1, 18750, 20000),
    (4, 1000, 5, 1, 21250,  2500),
    ]

#now grid tune us some lambdas!
grid_jobs = []
for (tname, training) in [("train", "true"), ("test", "false")]:
  for (uniw, odw, uww) in lambda_settings:
    lambda_arg = " --uniw=%.2f --odw=%.2f --uww=%.2f" % (uniw, odw, uww)
    for (fold, unimu, nodmu, nuwmu, odmu, uwmu) in split_settings:
      # just give all mus to everything of the same split:
      mu_arg = "--mu=%.1f --unimu=%.1f --nodmu=%.1f --nuwmu=%.1f --odmu=%.1f --uwmu=%.1f " % (unimu, unimu, nodmu, nuwmu, odmu, uwmu)
      fold_arg = " --split=%d " % fold
      for method in ["nbsdm", "nsdm", "gsdm", "msdm"]:
        method_arg = " --method=%s" % method
        output = "%s.grid_%.2f_%.2f_%.2f_split%d.%s.trecrun" % (method, uniw, odw, uww, fold, tname)
        grid_jobs += [output]
        print(output+":")
        print("\t%s %s %s %s %s --train=%s --output=$@" % (gridLearnJob, mu_arg, lambda_arg, fold_arg, method_arg, training))
        print

# generate qrels by split for the RankLib
for fold in [0,1,2,3,4]:
    for tsplit, training in [("train", "true"), ("test", "false")]:
        output = "judgments.%s.split%d.qrel" % (tsplit, fold)
        print(output+": "+qrels)
        print("\t%s --qrels=$< --train=%s --split=%d --output=$@" % (GEN_QRELS, training, fold))

feature_jobs = []
ranklib_jobs = []
trecrun_jobs = []
grouped_jobs = []

for tsplit, training in [("train", "true"), ("test", "false")]:
    for gen, genbool in [("gen", "true"), ("log", "false")]:
        for method in ["msdm", "nsdm"]:
            for (fold, unimu, nodmu, nuwmu, odmu, uwmu) in split_settings:
                # just give all mus to everything of the same split:
                mu_arg = "--mu=%.1f --unimu=%.1f --nodmu=%.1f --nuwmu=%.1f --odmu=%.1f --uwmu=%.1f " % (unimu, unimu, nodmu, nuwmu, odmu, uwmu)
                fold_arg = " --split=%d " % fold

                # writing rules
                feature_output = "%s.%s.%s.split%d.ranksvm" % (gen, method, tsplit, fold)
                feature_jobs += [feature_output]
                input_qrels = "judgments.%s.split%d.qrel" % (tsplit, fold)
                cmd = "\t${PREFIX} ${JAVA} ${JOPT} -cp ${JAR} jfoley.sdm.prob.experiment.RanklibExperiment --robust=%s --queries=%s" % (index, queries)

                # print feature_jobs
                print(feature_output+": "+input_qrels)
                print("\t%s --qrels=$< --train=%s --generative=%s --method=%s %s %s --output=$@ " % (cmd, training, genbool, method, mu_arg, fold_arg))
                print

                # don't generate models on test data:
                if (tsplit == "train"):
                    model_output = "%s.%s.%s.split%d.model" % (gen, method, tsplit, fold)
                    ranklib_jobs += [model_output]
                    # print ranklib_jobs
                    print(model_output+": "+feature_output)
                    print("\t%s -qrel %s -metric2t map -train $< -save $@ " % (RANKLIB, input_qrels))
                    print

                    # don't generate trecrun files on train data:
                    # print trecrun_jobs
                    indri_output = "%s.%s.test.split%d.trecrun" % (gen, method, fold)
                    model_output = "%s.%s.train.split%d.model" % (gen, method, fold)
                    trecrun_jobs += [indri_output]
                    test_feature_output = "%s.%s.test.split%d.ranksvm" % (gen, method, fold)
                    print(indri_output+": "+model_output+" "+test_feature_output)
                    print("\t%s -load %s -rank %s -indri $@ " % (RANKLIB, model_output, test_feature_output))
                    print

            # not split-based
            if (tsplit == "test"):
                indri_outputs = ' '.join(["%s.%s.test.split%d.trecrun" % (gen, method, fold) for fold in [0,1,2,3,4]])
                total_output = "%s.%s.trecrun" % (gen, method)
                grouped_jobs += [total_output]
                print(total_output+": ")
                print("\tcat %s > $@ " % indri_outputs)
                print


print(".PHONY: feature_jobs")
print("feature_jobs: " + ' '.join(feature_jobs))
print
print(".PHONY: ranklib_jobs")
print("ranklib_jobs: " + ' '.join(ranklib_jobs))
print
print(".PHONY: trecrun_jobs")
print("trecrun_jobs: " + ' '.join(trecrun_jobs))
print
print(".PHONY: grouped_jobs")
print("grouped_jobs: " + ' '.join(grouped_jobs))
print

print(".PHONY: grid_jobs")
print("grid_jobs: " + ' '.join(grid_jobs))
print

print(".PHONY: unigram_jobs")
print("unigram_jobs: " + ' '.join(unigram_jobs))
print

print(".PHONY: ngram_bigram_jobs")
print("ngram_bigram_jobs: " + ' '.join(ngram_bigram_jobs))
print

print(".PHONY: regular_bigram_jobs")
print("regular_bigram_jobs: " + ' '.join(regular_bigram_jobs))
print

